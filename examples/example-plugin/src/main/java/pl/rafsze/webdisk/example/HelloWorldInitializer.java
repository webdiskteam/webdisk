package pl.rafsze.webdisk.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.auto.service.AutoService;

import pl.rafsze.webdisk.api.plugin.PluginInitializer;

@AutoService(PluginInitializer.class) // this is recommended way to do this, but you can also manually add META-INF service entry (look at stackoverflow how to do this)
public class HelloWorldInitializer extends PluginInitializer
{
	private final Logger log = LoggerFactory.getLogger(HelloWorldInitializer.class); // you can also use lombok's @Slf4J
	
	@Override
	public void initialize()
	{
		// Here we're doing some plugin setup stuff, e.g. read config, connect to database etc.
		
		log.info("HelloWorld example plugin loaded");
	}
	
	@Override
	public void cleanup()
	{
		// Here we're cleaning up mess that we have made
	}
}
