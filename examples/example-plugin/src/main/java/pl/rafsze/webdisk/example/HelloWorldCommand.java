package pl.rafsze.webdisk.example;

import com.google.auto.service.AutoService;

import pl.rafsze.webdisk.api.command.CommandContext;
import pl.rafsze.webdisk.api.command.CommandHandler;
import pl.rafsze.webdisk.api.command.WebdiskCommand;

@WebdiskCommand({"hello", "hi"})
@AutoService(CommandHandler.class)
public class HelloWorldCommand extends CommandHandler
{
	@Override
	public void execute(CommandContext ctx)
	{
		ctx.getOutputStream().println("Hello");
		
		if ( ctx.getArgsCount() > 0 )
		{
			ctx.getOutputStream().println(String.join(" ", ctx.getArgs()));
		}
	}
}
