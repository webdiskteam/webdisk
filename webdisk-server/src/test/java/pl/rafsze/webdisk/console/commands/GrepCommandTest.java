package pl.rafsze.webdisk.console.commands;

import org.junit.Assert;
import org.junit.Test;


public class GrepCommandTest
{
	@Test
	public void testGrepCommand()
	{
		String test = "Budownictwo wodne i ladowe";
		String pattern = "wodne";
		
		String result = GrepCommand.grepLine(test, pattern);
		
		Assert.assertEquals("Budownictwo §cwodne§f i ladowe", result);
		
		
	}
	
	@Test
	public void testNonMatchingString()
	{
		String test2 = "Schody do nieba";
		String pattern2 = "pieklo";
		
		String result2 = GrepCommand.grepLine(test2, pattern2);
		Assert.assertEquals(null, result2);
	}
}
