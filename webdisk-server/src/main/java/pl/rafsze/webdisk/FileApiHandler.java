package pl.rafsze.webdisk;

import java.io.File;

import pl.rafsze.webdisk.files.FileInfo;
import pl.rafsze.webdisk.files.FileInfoResponse;
import pl.rafsze.webdisk.user.User;
import spark.Request;
import spark.Response;

public class FileApiHandler
{
	public Object getFileInfo(Request request, Response response)
	{
		response.type("application/json");
		
		User user = request.attribute("user");
		if ( user == null )
		{
			return FileInfoResponse.requireAuth();
		}
		
		// TODO: validate file path e.g contains '..', prevent user to escape his home directory
		String path = request.splat().length == 0 ? "/" : request.splat()[0];
		path = path.replace('\\', '/');
		//path = OperatingSystems.normalizePathForOperatingSystem(path);
		
		
		FileInfo fileInfo = FileInfo.of(new File(user.getHomeFolder(), path));
		/*
		if ( path.equals("/") && OperatingSystems.isWindows() )
		{
			fileInfo = FileInfo.ofRoots();
		}
		else
		{
			fileInfo = FileInfo.of(new File(path));
		}*/
		
		if ( fileInfo == null )
		{
			return FileInfoResponse.notExist();
		}
		
		return FileInfoResponse.fromFileInfo(fileInfo);
	}
}
