package pl.rafsze.webdisk;

import java.io.File;
import java.io.IOException;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import net.rafkos.utils.ResourceUnpacker.ResourceUnpacker;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Main
{
	public static void main(String[] args)
	{
        ResourceUnpacker.copyConfigFilesFromJarIfMissing(Main.class, new File("misc/"), "misc", "default.json");
	
		new WebDisk().init();
	}
}
