package pl.rafsze.webdisk.response;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode(callSuper = false)
@ToString
public class NotFoundError extends JsonResponse
{
	private final String error;
	
	public NotFoundError()
	{
		super(false);
		this.error = "NotFound";
	}
	
}
