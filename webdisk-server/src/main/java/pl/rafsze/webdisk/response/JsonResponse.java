package pl.rafsze.webdisk.response;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import spark.ResponseTransformer;

@Getter
@Setter
@ToString(callSuper = false)
@EqualsAndHashCode
@AllArgsConstructor
public class JsonResponse
{
	private static final JsonResponseRederer renderer = new JsonResponseRederer();
	
	private boolean success;
	
	public static ResponseTransformer renderer()
	{
		return renderer;
	}
}

@NoArgsConstructor(access = AccessLevel.PACKAGE)
class JsonResponseRederer implements ResponseTransformer
{
	private final Gson gson = new Gson();
	
	@Override
	public String render(Object model) throws Exception
	{
		Preconditions.checkState(model instanceof JsonResponse, "Model is not instance of json response");
		return gson.toJson(model);
	}
}
