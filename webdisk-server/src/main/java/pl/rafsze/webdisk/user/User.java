package pl.rafsze.webdisk.user;

import java.io.File;

public interface User extends pl.rafsze.webdisk.api.user.User
{
	String getUsername();
	
	String getDisplayName();
	
	String getPasswordHash();
	
	File getHomeFolder();
	
	boolean isHomeFolderWriteable();
	
	boolean isGuest();
	
}
