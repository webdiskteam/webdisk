package pl.rafsze.webdisk.user;

public interface UserManager
{
	User getUser(String username);
	
	User getGuestUser();
}
