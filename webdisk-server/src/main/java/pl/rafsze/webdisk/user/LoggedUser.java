/*
	
Copyright (C) 2018-2019 WebDisk Team

This program is free software: you can redistribute it and/or modify
It under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>
    
*/
package pl.rafsze.webdisk.user;

import java.io.File;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class LoggedUser implements User
{
	private LoggedUserConfig userConfig;
	
	@Override
	public String getUsername()
	{
		return userConfig.getUsername();
	}

	@Override
	public String getDisplayName()
	{
		return userConfig.getDisplayName();
	}

	@Override
	public String getPasswordHash()
	{
		return userConfig.getPassword();
	}

	@Override
	public File getHomeFolder()
	{
		return new File(userConfig.getHome());
	}

	@Override
	public boolean isHomeFolderWriteable()
	{
		return userConfig.isHomeWriteable();
	}

	@Override
	public boolean isGuest()
	{
		return false;
	}
	
}
