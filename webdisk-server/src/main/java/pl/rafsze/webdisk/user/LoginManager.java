package pl.rafsze.webdisk.user;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LoginManager
{
	private final UserManager userManager;
	
	public User tryLoginUser(String username, String passwordHash)
	{
		User user = userManager.getUser(username);
		
		if ( user == null || !user.getPasswordHash().equalsIgnoreCase(passwordHash) )
		{
			return null;
		}
		
		return user;
	}
	
	public User getGuestUser()
	{
		return userManager.getGuestUser();
	}
}
