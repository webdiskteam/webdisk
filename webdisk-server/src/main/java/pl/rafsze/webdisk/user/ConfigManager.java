/*
	
Copyright (C) 2018-2019 WebDisk Team

This program is free software: you can redistribute it and/or modify
It under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>
    
*/
package pl.rafsze.webdisk.user;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import pl.rafsze.webdisk.misc.EHashMethodType;
import pl.rafsze.webdisk.misc.PasswordHashBuilder;

public class ConfigManager implements UserManager
{
	private static final Gson gson = new Gson();
	private File sourceFile;
	@Getter
	private File dataFolder;
	private ConfigHolder configHolder;
	private GuestUser guestUser;
	private HashMap<String, LoggedUser> users = new HashMap<>();
	
	@SneakyThrows
	public ConfigManager(File source)
	{
		this.sourceFile = source;
		init();
	}

	private void init() throws JsonSyntaxException, JsonIOException, FileNotFoundException
	{
		if(!sourceFile.isFile())
		{
			throw new IllegalArgumentException("Config file is not a file.");
		}
		configHolder = gson.fromJson(new FileReader(sourceFile), ConfigHolder.class);
		for(LoggedUserConfig loggedUserConfig:configHolder.getUserConfigs())
		{
			// hash password if hash method is "none"
			if(loggedUserConfig.getRawPassword().toUpperCase().startsWith("NONE:"))
			{
				String[] args = loggedUserConfig.getRawPassword().split(":");
				EHashMethodType hashMethod = EHashMethodType.valueOf(args[0].toUpperCase());
				if(hashMethod == EHashMethodType.NONE)
				{
					String hashedPassword = PasswordHashBuilder.getHash(args[1], configHolder.getHashMethod());
					loggedUserConfig.setPassword(configHolder.getHashMethod()+":"+hashedPassword);
				}
			}
			LoggedUser user = new LoggedUser(loggedUserConfig);
			users.put(loggedUserConfig.getUsername(), user);
		}
		guestUser = new GuestUser(configHolder.getGuestConfig());
		
		dataFolder = new File(configHolder.getDataFolder());
		dataFolder.mkdirs();
		if(!dataFolder.exists() || !dataFolder.isDirectory())
		{
			throw new IllegalArgumentException("data folder "+dataFolder.getAbsolutePath()+"is unreachable.");
		}
	}
	
	public String getIpAddress()
	{
		return configHolder.getIpAddress();
	}
	
	public int getPort()
	{
		return configHolder.getPort();
	}
	
	public GuestUserConfig getGuestUserConfig()
	{
		return guestUser.getUserConfig();
	}
	
	/**
	 * Returns null if no guest account
	 */
	@Override
	public GuestUser getGuestUser()
	{
		if (guestUser.isEnabled())
		{
			return guestUser;
		}
		return null;
	}
	
	/**
	 * Returns a map of users where username is a key.
	 */
	public HashMap<String, LoggedUser> getUsersMap()
	{
		return users;
	}
	
	/**
	 * Returns User by given username, returns null otherwise
	 */
	@Override
	public User getUser(String username)
	{
		if(users.containsKey(username))
		{
			return users.get(username);
		}
		return null;
	}
	
	/**
	 * Saves current config to source file
	 */
	public boolean save()
	{
		LoggedUserConfig[] loggedUserConfig = new LoggedUserConfig[users.size()];
		int index = 0;
		for(User loggedUser:users.values())
		{
			loggedUserConfig[index++] = ((LoggedUser) loggedUser).getUserConfig();
		}
		configHolder.setUserConfigs(loggedUserConfig);
		configHolder.setGuestConfig(guestUser.getUserConfig());
		try
		{
			Writer writer = new FileWriter(sourceFile);
			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.create().toJson(configHolder, writer);
			writer.flush();
			writer.close();
		}catch(JsonIOException | IOException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	@Getter
	@Setter
	@AllArgsConstructor
	private static class ConfigHolder
	{
		private int port;
		private String ipAddress;
		private String dataFolder;
		private EHashMethodType hashMethod;
		private GuestUserConfig guestConfig;
		private LoggedUserConfig[] userConfigs;
	}
}
