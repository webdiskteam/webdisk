package pl.rafsze.webdisk.console.commands;

import pl.rafsze.webdisk.api.command.CommandContext;
import pl.rafsze.webdisk.api.command.CommandHandler;
import pl.rafsze.webdisk.api.command.WebdiskCommand;

@WebdiskCommand({"echo", "print"})
public class EchoCommand extends CommandHandler
{
	@Override
	public void execute(CommandContext ctx)
	{
		ctx.getOutputStream().println(String.join(" ", ctx.getArgs()));
	}
}
