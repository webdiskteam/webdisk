package pl.rafsze.webdisk.console;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Preconditions;

import pl.rafsze.webdisk.api.command.CommandContext;
import pl.rafsze.webdisk.api.command.CommandHandler;
import pl.rafsze.webdisk.api.command.WebdiskCommand;
import pl.rafsze.webdisk.console.commands.CatCommand;
import pl.rafsze.webdisk.console.commands.EchoCommand;
import pl.rafsze.webdisk.console.commands.GrepCommand;
import pl.rafsze.webdisk.console.commands.ListCommand;
import pl.rafsze.webdisk.user.User;

public class ConsoleCommandManager
{
	private final Map<String, CommandHandler> commandsByName = new HashMap<>();
	
	public void registerBaseCommands()
	{
		registerCommandHandler(new EchoCommand());
		registerCommandHandler(new CatCommand());
		registerCommandHandler(new ListCommand());
		registerCommandHandler(new GrepCommand());
	}
	
	public void registerCommandHandler(CommandHandler commandHandler)
	{
		System.out.println(Arrays.toString(commandHandler.getClass().getAnnotations()));
		WebdiskCommand commandAnnotation = commandHandler.getClass().getAnnotation(WebdiskCommand.class);
		
		Preconditions.checkState(commandAnnotation != null, "CommandHandler must be annotated with WebdiskCommand annotation");
		
		for ( String commandName : commandAnnotation.value() )
		{
			commandsByName.put(commandName, commandHandler);
		}
	}
	
	public void executeCommand(String commandLine, String workingDirectory, User user, InputStream input, PrintStream output, PrintStream error)
	{
		String[] split = commandLine.split(" ");
		
		if ( split.length == 0 )
		{
			error.println("No command!");
			return;
		}
		
		String command = split[0];
		String[] args = new String[split.length - 1];
		System.arraycopy(split, 1, args, 0, split.length - 1);
		
		CommandHandler handler = commandsByName.get(command);
		if ( handler == null )
		{
			error.println("Unknow command!");
			return;
		}
		
		CommandContext ctx = new WebdiskCommandContext(user, commandLine, command, args, workingDirectory, input, output, error);
		
		try
		{
			handler.execute(ctx);
		}
		catch ( Throwable e )
		{
			error.println("§cInternal error occured when executing command");
			e.printStackTrace();
		}
	}
}
