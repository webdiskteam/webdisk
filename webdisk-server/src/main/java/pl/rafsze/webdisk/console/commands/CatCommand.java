package pl.rafsze.webdisk.console.commands;

import java.io.FileInputStream;

import lombok.SneakyThrows;
import pl.rafsze.webdisk.api.command.CommandContext;
import pl.rafsze.webdisk.api.command.CommandHandler;
import pl.rafsze.webdisk.api.command.WebdiskCommand;
import spark.utils.IOUtils;

@WebdiskCommand("cat")
public class CatCommand extends CommandHandler
{
	@Override
	@SneakyThrows
	public void execute(CommandContext ctx)
	{
		if ( ctx.getArgsCount() == 0 )
		{
			ctx.getErrorStream().println("Usage: " + ctx.getCommandName() + " <file>");
			return;
		}
		
		String file = ctx.getArg(0);
		
		FileInputStream fis = new FileInputStream(ctx.getUser().getFile(file));
		
		IOUtils.copy(fis, ctx.getOutputStream());
	}
}
