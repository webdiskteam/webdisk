package pl.rafsze.webdisk.console;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import pl.rafsze.webdisk.WebDisk;

@WebSocket
public class ConsoleWebSocketHandler
{
	private static Map<Session, ConsoleSession> sessions = new HashMap<>();
	
	@OnWebSocketConnect
	public void onWebsocketConnect(Session session)
	{
		sessions.put(session, new ConsoleSession(WebDisk.getInstance().getConfigManager().getGuestUser(), session));
	}
	
	@OnWebSocketClose
	public void onWebsocketDisconnect(Session session, int statusCode, String reason)
	{
		ConsoleSession consoleSession = sessions.get(session);
		consoleSession.onDisconnect();
	}
	
	@OnWebSocketMessage
	public void onWebsocketMessage(Session session, String message)
	{
		ConsoleSession consoleSession = sessions.get(session);
		consoleSession.onMessage(message);
	}
}
