package pl.rafsze.webdisk.console.commands;

import java.io.File;

import pl.rafsze.webdisk.api.command.CommandContext;
import pl.rafsze.webdisk.api.command.CommandHandler;
import pl.rafsze.webdisk.api.command.WebdiskCommand;
import pl.rafsze.webdisk.console.WebdiskCommandContext;

@WebdiskCommand({"list", "ls"})
public class ListCommand extends CommandHandler
{
	@Override
	public void execute(CommandContext ctx)
	{
		WebdiskCommandContext context = (WebdiskCommandContext) ctx;
		
		File file = ctx.getUser().getFile(context.getWorkingDirectory());
		
		for ( File f : file.listFiles() )
		{
			ctx.getOutputStream().println(f.getName());
		}
	}
}
