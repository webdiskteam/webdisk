package pl.rafsze.webdisk.console.commands;

import java.io.File;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.SneakyThrows;
import pl.rafsze.webdisk.api.command.CommandContext;
import pl.rafsze.webdisk.api.command.CommandHandler;
import pl.rafsze.webdisk.api.command.WebdiskCommand;
import pl.rafsze.webdisk.console.WebdiskCommandContext;

@WebdiskCommand("grep")
public class GrepCommand extends CommandHandler
{

	@Override
	@SneakyThrows
	public void execute(CommandContext ctx)
	{
		if ( ctx.getArgsCount() < 2 )
		{
			ctx.getErrorStream().println("Usage: " + ctx.getCommandName() + " <file> <pattern>");
			return;
		}
		
		String pattern = ctx.getArg(1);
		
		WebdiskCommandContext context = (WebdiskCommandContext) ctx;
		
		File file = ctx.getUser().getFile(context.getWorkingDirectory() + "/" + ctx.getArg(0));
		
		try ( Scanner sc = new Scanner(file) )
		{
			while ( sc.hasNextLine() )
			{
				String result = grepLine(sc.nextLine(), pattern);
				if ( result != null )
				{
					ctx.getOutputStream().println(result);
				}
			}
		}
	}
	
	public static String grepLine(String line, String pattern)
	{
		Matcher m = Pattern.compile(pattern).matcher(line);

		boolean found = false;
		StringBuffer sb = new StringBuffer(line.length());
		while ( m.find() )
		{
			String text = m.group(0);
			m.appendReplacement(sb, "§c" + Matcher.quoteReplacement(text) + "§f");
			found = true;
		}
		
		m.appendTail(sb);
		return found ? sb.toString() : null;
	}
}
