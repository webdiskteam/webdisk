package pl.rafsze.webdisk.console;

import java.io.IOException;
import java.io.OutputStream;

import org.eclipse.jetty.websocket.api.RemoteEndpoint;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class WebSocketOutputStream extends OutputStream
{
	private final RemoteEndpoint remoteEndpoint;
	
	private final byte[] buffer = new byte[128];
	private int bufferPointer = 0;
	
	private final String colorCode;
	
	public WebSocketOutputStream(RemoteEndpoint remote)
	{
		this(remote, "§f");
	}
	
	@Override
	public void write(int b) throws IOException
	{
		buffer[bufferPointer++] = (byte) b;
		
		if ( b == '\n' || bufferPointer == buffer.length )
		{
			flush();
		}
	}
	
	@Override
	public void flush() throws IOException
	{
		String message = new String(buffer, 0, bufferPointer);
		remoteEndpoint.sendString(colorCode + message);
		bufferPointer = 0;
	}
}
