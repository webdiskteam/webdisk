package pl.rafsze.webdisk.console;

import java.io.PrintStream;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.jetty.websocket.api.Session;

import com.google.common.base.Preconditions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.rafsze.webdisk.WebDisk;
import pl.rafsze.webdisk.user.User;

@Getter
@RequiredArgsConstructor
public class ConsoleSession
{
	private final User user;
	private final Session session;
	
	private Thread commandThread;
	
	private AtomicBoolean arrived = new AtomicBoolean();
	
	public void onMessage(String message)
	{
		if ( arrived.getAndSet(true) )
		{
			return;
		}
		
		String[] split = message.split("\0");
		Preconditions.checkState(split.length == 2);
		
		String currentDirectory = split[0];
		String commandLine = split[1];
		
		PrintStream output = new PrintStream(new WebSocketOutputStream(session.getRemote()));
		PrintStream error = new PrintStream(new WebSocketOutputStream(session.getRemote(), "§c"));
		
		WebDisk.getInstance().getCommandManager().executeCommand(commandLine, currentDirectory, user, null, output, error);
		session.close();
	}
	
	public void onDisconnect()
	{
		
	}
}
