package pl.rafsze.webdisk.console;

import java.io.InputStream;
import java.io.PrintStream;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.rafsze.webdisk.api.command.CommandContext;
import pl.rafsze.webdisk.api.user.User;

@Getter
@RequiredArgsConstructor
public class WebdiskCommandContext implements CommandContext
{
	private final User user;
	private final String commandLine;
	
	private final String commandName;
	private final String[] args;

	private final String workingDirectory;
	
	private final InputStream inputStream;
	private final PrintStream outputStream;
	private final PrintStream errorStream;
	
	@Override
	public String getArg(int index)
	{
		return args[index];
	}

	@Override
	public void halt()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getArgsCount()
	{
		return args.length;
	}	
}
