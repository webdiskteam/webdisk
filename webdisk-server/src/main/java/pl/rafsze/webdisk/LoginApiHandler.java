package pl.rafsze.webdisk;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import pl.rafsze.webdisk.response.JsonResponse;
import pl.rafsze.webdisk.user.LoginManager;
import pl.rafsze.webdisk.user.User;
import spark.Request;
import spark.Response;

@RequiredArgsConstructor
public class LoginApiHandler
{
	private final Gson gson = new Gson();
	private final LoginManager loginManager;
	
	public Object handleLogin(Request request, Response response)
	{
		response.type("application/json");
		
		request.session(true);
		Preconditions.checkState(request.session().attribute("user") == null);
		
		LoginInput input = gson.fromJson(request.body(), LoginInput.class);
		
		User user = loginManager.tryLoginUser(input.getUsername(), input.getPassword());
		
		if ( user == null )
		{
			return new LoginResponse(false);
		}
		
		request.session().attribute("user", user);
		return new LoginResponse(true);
	}
	
	public Object handleLogout(Request request, Response response)
	{
		response.type("application/json");
		request.session().invalidate();
		
		return new LogoutResponse();
	}
	
	public Object handleProfile(Request request, Response response)
	{
		response.type("application/json");
		return ProfileResponse.forUser((User) request.attribute("user"));
	}
	
	public void prepareUser(Request request, Response response)
	{
		request.session(true);
		User user = request.session().attribute("user");
		
		if ( user == null )
		{
			user = loginManager.getGuestUser();
		}
		
		if ( user != null )
		{
			request.attribute("user", user);
		}
	}
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class LoginInput
	{
		private String username;
		private String password;
	}
	
	@Data
	@EqualsAndHashCode(callSuper = false)
	public static class LoginResponse extends JsonResponse
	{
		private boolean loginSuccess;
		
		public LoginResponse(boolean loginSuccess)
		{
			super(true);
			this.loginSuccess = loginSuccess;
		}
	}
	
	@Data
	@EqualsAndHashCode(callSuper = false)
	@ToString
	public static class LogoutResponse extends JsonResponse
	{
		public LogoutResponse()
		{
			super(true);
		}
	}
	
	public static class ProfileResponse extends JsonResponse
	{
		private ProfileInfo userProfile;
		
		private ProfileResponse(ProfileInfo userProfile)
		{
			super(true);
			this.userProfile = userProfile;
		}
		
		public static ProfileResponse forUser(User user)
		{
			if ( user == null )
			{
				return new ProfileResponse(null);
			}
			
			return new ProfileResponse(new ProfileInfo(user.getUsername(), user.getDisplayName(), user.isGuest()));
		}
		
		@RequiredArgsConstructor
		@Data
		private static class ProfileInfo
		{
			private final String username;
			private final String displayName;
			private final boolean isGuest;
		}
	}
}
