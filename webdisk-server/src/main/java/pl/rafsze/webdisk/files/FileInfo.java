package pl.rafsze.webdisk.files;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class FileInfo
{
	private final boolean directory;
	private final @NonNull String name;
	private final long size;
	private final long modificationTime;
	
	private final List<FileInfo> directoryContents;
	
	public static FileInfo ofRoots()
	{
		File[] roots = File.listRoots();
		FileInfo[] rootsInfo = new FileInfo[roots.length];
		
		for ( int i = 0; i < roots.length; i++ )
		{
			String rootPath = roots[i].getAbsolutePath();
			String rootName = rootPath.substring(0, rootPath.length() - 1);
			
			rootsInfo[i] = new FileInfo(true, rootName, -1, -1, null);
		}
		
		return new FileInfo(true, "/", -1, -1, Arrays.asList(rootsInfo));
	}
	
	public static FileInfo of(@NonNull File file)
	{
		return ofFileOrDirectory(file, true);
	}
	
	private static FileInfo ofFileOrDirectory(File fileOrDirectory, boolean includeDirectoryContent)
	{
		FileInfo result;
		if ( fileOrDirectory.isDirectory() )
		{
			result = ofDirectory(fileOrDirectory, includeDirectoryContent);
		}
		else
		{
			result = ofFile(fileOrDirectory);
		}
		
		return fileOrDirectory.exists() ? result : null; // existence check is in case file has been deleted while preparing file info.
	}
	
	private static FileInfo ofFile(File file)
	{
		return new FileInfo(false, file.getName(), file.length(), file.lastModified(), null);
	}
	
	private static FileInfo ofDirectory(File directory, boolean includeDirectoryContent)
	{
		return new FileInfo(true, directory.getName(), -1, -1, includeDirectoryContent ? getDirectoryContent(directory) : null);
	}
	
	private static List<FileInfo> getDirectoryContent(File directory)
	{
		File[] files = directory.listFiles();
		if ( files == null )
		{
			return null;
		}
		
		List<FileInfo> content = new ArrayList<>(files.length);
		
		for ( int i = 0; i < files.length; i++ )
		{
			FileInfo info = ofFileOrDirectory(files[i], false);
			if ( info != null )
			{
				content.add(info);
			}
		}
		
		return content;
	}
}
