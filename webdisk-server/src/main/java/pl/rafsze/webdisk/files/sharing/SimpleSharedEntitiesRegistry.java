/*
	
Copyright (C) 2018-2019 WebDisk Team

This program is free software: you can redistribute it and/or modify
It under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>
    
*/
package pl.rafsze.webdisk.files.sharing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;
import pl.rafsze.webdisk.user.User;

public class SimpleSharedEntitiesRegistry
{
	private final Map<String, ArrayList<ISharedEntity>> userToSharedEntitiesSetMap = new HashMap<>();
	private final Map<UUID, ISharedEntity> sharedEntitiesById = new HashMap<>();

	@Getter
	@Setter
	private IRegistryLoader<Map<String, ArrayList<ISharedEntity>>> registryLoader;

	public ISharedEntity getSharedEntityById(UUID id)
	{
		return sharedEntitiesById.get(id);
	}

	public SimpleSharedEntitiesRegistry(IRegistryLoader<Map<String, ArrayList<ISharedEntity>>> registryLoader)
	{
		this.registryLoader = registryLoader;
	}

	public ArrayList<ISharedEntity> getUserShares(User user)
	{
		return userToSharedEntitiesSetMap.get(user.getUsername());
	}

	public boolean registerSharedEntity(User entityOwner, ISharedEntity sharedEntity)
	{
		if(!userToSharedEntitiesSetMap.containsKey(entityOwner.getUsername()))
		{
			userToSharedEntitiesSetMap.put(entityOwner.getUsername(), new ArrayList<>());
		}

		sharedEntitiesById.put(sharedEntity.getId(), sharedEntity);
		return userToSharedEntitiesSetMap.get(entityOwner.getUsername()).add(sharedEntity);
	}

	public boolean unregisterSharedEntity(User entityOwner, ISharedEntity sharedEntity)
	{
		if(!userToSharedEntitiesSetMap.containsKey(entityOwner.getUsername()))
		{
			return false;
		}

		sharedEntitiesById.remove(sharedEntity.getId());
		return userToSharedEntitiesSetMap.get(entityOwner.getUsername()).remove(sharedEntity);
	}

	public boolean unregisterSharedEntity(ISharedEntity sharedEntity)
	{
		for(ArrayList<ISharedEntity> set : userToSharedEntitiesSetMap.values())
		{
			if(set.remove(sharedEntity))
			{
				sharedEntitiesById.remove(sharedEntity.getId());
				return true;
			}
		}
		return false;
	}

	public boolean unregisterUser(User entityOwner)
	{
		if(!userToSharedEntitiesSetMap.containsKey(entityOwner.getUsername()))
		{
			return false;
		}
		userToSharedEntitiesSetMap.remove(entityOwner.getUsername());
		return true;
	}

	public boolean loadData()
	{
		try
		{
			userToSharedEntitiesSetMap.clear();
			userToSharedEntitiesSetMap.putAll(registryLoader.loadData());

			for(ArrayList<ISharedEntity> sharedEntities : userToSharedEntitiesSetMap.values())
			{
				for(ISharedEntity entity : sharedEntities)
				{
					sharedEntitiesById.put(entity.getId(), entity);
				}
			}

			return true;
		}catch(Exception e)
		{
			return false;
		}
	}

	public boolean saveData()
	{
		return registryLoader.saveData(userToSharedEntitiesSetMap);
	}

}
