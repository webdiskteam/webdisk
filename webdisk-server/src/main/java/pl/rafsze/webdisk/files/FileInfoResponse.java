package pl.rafsze.webdisk.files;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import pl.rafsze.webdisk.response.JsonResponse;

@EqualsAndHashCode(callSuper = false)
@ToString
@Getter
public class FileInfoResponse extends JsonResponse
{
	private final boolean fileExist;
	private final boolean requireAuth;
	private final FileInfo file;
	
	private FileInfoResponse(boolean fileExist, boolean requireAuth, FileInfo file)
	{
		super(true);
		this.fileExist = fileExist;
		this.requireAuth = requireAuth;
		this.file = file;
	}
	
	public static FileInfoResponse requireAuth()
	{
		return new FileInfoResponse(false, true, null);
	}
	
	public static FileInfoResponse notExist()
	{
		return new FileInfoResponse(false, false, null);
	}
	
	public static FileInfoResponse fromFileInfo(FileInfo fileInfo)
	{
		return new FileInfoResponse(true, false, fileInfo);
	}
}
