/*
	
Copyright (C) 2018-2019 WebDisk Team

This program is free software: you can redistribute it and/or modify
It under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>
    
*/
package pl.rafsze.webdisk.files.sharing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import pl.rafsze.webdisk.WebDisk;

@Getter
@Setter
public class JsonRegistryLoader implements IRegistryLoader<Map<String, ArrayList<ISharedEntity>>>
{
	private static final Gson gson;
	private static String filename = "sharedFilesRegistry.json";
	static {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();
		gson = builder.create();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@SneakyThrows
	public HashMap<String, ArrayList<ISharedEntity>> loadData()
	{
		HashMap<String, ArrayList<ISharedEntity>> loadedMap;
		File source = new File(WebDisk.getInstance().getConfigManager().getDataFolder(), filename);
		if(source.exists())
		{
			FileReader in = null;
			try
			{
				in = new FileReader(source);
				loadedMap = gson.fromJson(in, HashMap.class);
				return loadedMap;
			}
			catch ( FileNotFoundException e )
			{
				e.printStackTrace();
			}
			finally
			{
				in.close();
			}
		}
		
		return new HashMap<>();
	}

	@Override
	@SneakyThrows
	public boolean saveData(Map<String, ArrayList<ISharedEntity>> data)
	{
		File destination = new File(WebDisk.getInstance().getConfigManager().getDataFolder(), filename);
		FileWriter out = null;
		try
		{
			out = new FileWriter(destination);
			gson.toJson(data, out);
			return true;
		}catch(IOException e)
		{
			e.printStackTrace();
		}finally
		{
			out.close();
		}
		return false;
	}

}
