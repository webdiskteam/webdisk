/*
	
Copyright (C) 2018-2019 WebDisk Team

This program is free software: you can redistribute it and/or modify
It under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>
    
*/
package pl.rafsze.webdisk.files.sharing;

import java.io.File;
import java.util.HashMap;
import java.util.UUID;

import lombok.Getter;
import pl.rafsze.webdisk.user.User;

public class SharedFile implements ISharedEntity
{
	@Getter
	private final UUID id;
	
	private File file;
	@Getter
	private String filePath;
	private HashMap<String, ShareRight> sharedRightsMap = new HashMap<>();
	
	public SharedFile(UUID id, String filePath, File file)
	{
		this.id = id;
		this.file = file;
	}
	
	@Override
	public boolean isPublic()
	{
		return sharedRightsMap.size() == 0;
	}

	@Override
	public HashMap<String, ShareRight> getShareRightsMap()
	{
		return sharedRightsMap;
	}

	@Override
	public File getFile()
	{
		return file;
	}

	@Override
	public ShareRight getUserShareRight(User user)
	{
		if(!sharedRightsMap.containsKey(user.getUsername()))
		{
			return null;
		}
		return sharedRightsMap.get(user.getUsername());
	}

	@Override
	public boolean addShareRight(User user, ShareRight shareRight)
	{
		if(sharedRightsMap.containsKey(user.getUsername()))
		{
			return false;
		}
		sharedRightsMap.put(user.getUsername(), shareRight);
		return true;
	}

	@Override
	public boolean removeShareRight(User user)
	{
		if(sharedRightsMap.containsKey(user.getUsername()))
		{
			return false;
		}
		sharedRightsMap.remove(user.getUsername());
		return true;
	}

	@Override
	public void setShareRight(User user, ShareRight shareRight)
	{
		sharedRightsMap.put(user.getUsername(), shareRight);
	}

}
