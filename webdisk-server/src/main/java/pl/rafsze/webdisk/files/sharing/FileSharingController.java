package pl.rafsze.webdisk.files.sharing;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import pl.rafsze.webdisk.files.FileInfo;
import pl.rafsze.webdisk.files.FileInfoResponse;
import pl.rafsze.webdisk.response.JsonResponse;
import pl.rafsze.webdisk.user.User;
import spark.Request;
import spark.Response;

@RequiredArgsConstructor
public class FileSharingController
{
	private final SimpleSharedEntitiesRegistry sharedEntitiesRegistry;
	private final Gson gson = new Gson();
	
	public Object shareFile(Request request, Response response)
	{
		response.type("application/json");
		
		User user = request.attribute("user");
		Preconditions.checkState(user != null);
		
//		if ( user.isGuest() )
//		{
//			return new JsonResponse(false);
//		}
		
		ShareFileInput input = gson.fromJson(request.body(), ShareFileInput.class);
		File file = user.getFile(input.getFilePath());
		SharedFile sharedFile = new SharedFile(UUID.randomUUID(), input.getFilePath(), file);
		
		for ( ShareUser shareUser : input.getUsers() )
		{
			sharedFile.getShareRightsMap().put(shareUser.getUsername(), new ShareRight(shareUser.isWriteable(), true));
		}
		
		sharedEntitiesRegistry.registerSharedEntity(user, sharedFile);
		return new ShareFileResponse(sharedFile.getId());
	}
	
	public Object removeShare(Request request, Response response)
	{
		response.type("application/json");
		
		RemoveShareInput input = gson.fromJson(request.body(), RemoveShareInput.class);
		User user = request.attribute("user");
		Preconditions.checkState(user != null);
		
		ISharedEntity sharedEntity = sharedEntitiesRegistry.getSharedEntityById(input.getUuid());
		
		return new JsonResponse(sharedEntitiesRegistry.unregisterSharedEntity(user, sharedEntity));
	}
	
	
	public Object getSharedFiles(Request request, Response response)
	{
		response.type("application/json");
		
		User user = request.attribute("user");
		Preconditions.checkState(user != null);
		
		ArrayList<ISharedEntity> userShares = sharedEntitiesRegistry.getUserShares(user);
		if ( userShares == null )
		{
			userShares = new ArrayList<>();
		}
		
		SharedFilesResponse responseData = new SharedFilesResponse();
		
		for ( ISharedEntity sharedEntity : userShares )
		{
			List<ShareUser> shareUsers = new ArrayList<>();
			
			for ( Entry<String, ShareRight> shareRight : sharedEntity.getShareRightsMap().entrySet() )
			{
				shareUsers.add(new ShareUser(shareRight.getKey(), shareRight.getValue().isWriteable()));
			}
			
			responseData.getSharedFiles().add(new SharedFileInfo(sharedEntity.getId(), shareUsers.toArray(new ShareUser[0]), sharedEntity.getFilePath()));
		}
		
		return responseData;
	}
	
	public Object getShare(Request request, Response response)
	{
		response.type("application/json");
		
		User user = request.attribute("user");
		if ( user == null )
		{
			return FileInfoResponse.requireAuth();
		}
		
		String path = request.splat().length == 0 ? "" : request.splat()[0];
		path = path.replace('\\', '/');

		ISharedEntity sharedEntity = sharedEntitiesRegistry.getSharedEntityById(UUID.fromString(request.params("uuid")));
		if ( sharedEntity == null 
				|| ( user != null && !sharedEntity.getShareRightsMap().isEmpty() && sharedEntity.getShareRightsMap().containsKey(user.getUsername()) ) )
		{
			return FileInfoResponse.notExist();
		}
		
		FileInfo fileInfo = FileInfo.of(new File(sharedEntity.getFile(), path));

		if ( fileInfo == null )
		{
			return FileInfoResponse.notExist();
		}
		
		return FileInfoResponse.fromFileInfo(fileInfo);
	}
	
	@Data
	@EqualsAndHashCode(callSuper = false)
	private static class ShareFileResponse extends JsonResponse
	{
		private UUID uuid;
		
		public ShareFileResponse(UUID uuid)
		{
			super(true);
			this.uuid = uuid;
		}
	}
	
	@NoArgsConstructor
	@AllArgsConstructor
	@Data
	private static class ShareUser
	{
		private String username;
		private boolean writeable;
	}
	
	@NoArgsConstructor
	@Data
	private static class ShareFileInput
	{
		private String filePath;
		private int expires;
		
		private ShareUser[] users;
	}
	
	@NoArgsConstructor
	@Data
	private static class RemoveShareInput
	{
		private UUID uuid;
	}
	
	@Data
	private static class SharedFileInfo
	{
		private UUID uuid;
		private String filePath;
		private ShareUser[] users;
		
		public SharedFileInfo(UUID uuid, ShareUser[] users, String filePath)
		{			
			this.uuid = uuid;
			this.filePath = filePath;
			this.users = users;
		}
	}
	
	@Data
	@EqualsAndHashCode(callSuper = false)
	private static class SharedFilesResponse extends JsonResponse
	{
		private final List<SharedFileInfo> sharedFiles = new ArrayList<>();
		
		public SharedFilesResponse()
		{
			super(true);
		}
	}
}
