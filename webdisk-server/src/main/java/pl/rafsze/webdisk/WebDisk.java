/*
	
Copyright (C) 2018-2019 WebDisk Team

This program is free software: you can redistribute it and/or modify
It under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>
    
*/
package pl.rafsze.webdisk;

import java.io.File;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import pl.rafsze.webdisk.console.ConsoleCommandManager;
import pl.rafsze.webdisk.console.ConsoleWebSocketHandler;
import pl.rafsze.webdisk.files.sharing.FileSharingController;
import pl.rafsze.webdisk.files.sharing.JsonRegistryLoader;
import pl.rafsze.webdisk.files.sharing.SimpleSharedEntitiesRegistry;
import pl.rafsze.webdisk.response.JsonResponse;
import pl.rafsze.webdisk.user.ConfigManager;
import pl.rafsze.webdisk.user.LoginManager;
import pl.rafsze.webdisk.utils.Routes;
import spark.Spark;

@Getter
@Slf4j
public class WebDisk
{
	private static WebDisk instance;
	private final FileApiHandler fileApiHandler = new FileApiHandler();
	private final ConfigManager configManager = new ConfigManager(new File("misc/default.json"));
	private final SimpleSharedEntitiesRegistry sharedEntitiesRegistry = new SimpleSharedEntitiesRegistry(new JsonRegistryLoader());
	private final LoginManager loginManager = new LoginManager(configManager);
	private final LoginApiHandler loginApiHandler = new LoginApiHandler(loginManager);
	private final UploadApiHandler uploadApiHandler = new UploadApiHandler();
	private final ConsoleCommandManager commandManager = new ConsoleCommandManager();
	private final DownloadFileHandler downloadHandler = new DownloadFileHandler();
	
	private final FileSharingController sharingController = new FileSharingController(sharedEntitiesRegistry);
	
	public static WebDisk getInstance()
	{
		return instance;
	}
	
	public WebDisk()
	{
		instance = this;
	}
	
	public void init()
	{
		sharedEntitiesRegistry.loadData();	
		initWebServer();
		log.info("WebDisk successfully initialized");
	}
	
	private void initWebServer()
	{
		// TODO: add way to configure this
		
		Spark.port(configManager.getPort());
		
		
		if ( System.getProperty("pl.rafsze.webdisk.dev.appfolder") != null )
		{
			File externalAppFolder = new File(System.getProperty("pl.rafsze.webdisk.dev.appfolder"));
			Spark.staticFiles.externalLocation(externalAppFolder.getAbsolutePath());
			Routes.setExternalResourcesFolder(externalAppFolder);
			
			log.info("Custom app folder location has been set to: {}", externalAppFolder.getAbsolutePath());
		}
		else
		{
			Spark.staticFiles.location("/app");
		}
		
		Spark.webSocket("/console", ConsoleWebSocketHandler.class);
		
		// TODO: better error pages
		Spark.notFound("<h1>404 NOT FOUND</h1><br><h3>It looks like we don't have what you want.</h3>");
		Spark.internalServerError("<h1>500 INTERNAL SERVER ERROR</h1><br><h3>It looks like we've got a problem.</h3>");
		
		setupFrontendAppRoutes();
		setupRestApiRoutes();
		
		commandManager.registerBaseCommands();

		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			public void run()
			{
				getConfigManager().save();
				getSharedEntitiesRegistry().saveData();
			}
		});
	}

	private void setupFrontendAppRoutes()
	{
		Spark.get("/", "text/html", Routes.redirect("/files/"));
		Spark.get("/files", "text/html", Routes.redirect("/files/"));
		Spark.get("/files/*", "text/html", Routes.staticResource("/index.html"));
		Spark.get("/shared/*", "text/html", Routes.staticResource("/index.html"));
	}
	
	private void setupRestApiRoutes()
	{
		Spark.before("/*", "application/json", loginApiHandler::prepareUser);
		
		Spark.get("/files/*", "application/json", fileApiHandler::getFileInfo, JsonResponse.renderer());
		Spark.get("/download/*", "application/json;application/octet-stream", downloadHandler::downloadFile, JsonResponse.renderer());
		Spark.put("/setContent/*", "application/json", uploadApiHandler::setFileContent, JsonResponse.renderer());
		
		Spark.post("/uploadFile", "multipart/form-data", uploadApiHandler::handleUpload, JsonResponse.renderer());
		
		Spark.post("/login", "application/json", loginApiHandler::handleLogin, JsonResponse.renderer());
		Spark.post("/logout", "application/json", loginApiHandler::handleLogout, JsonResponse.renderer());
		Spark.get("/profile", "application/json", loginApiHandler::handleProfile, JsonResponse.renderer());
		
		Spark.get("/sharedFiles", "application/json", sharingController::getSharedFiles, JsonResponse.renderer());
		Spark.post("/shareFile", "application/json", sharingController::shareFile, JsonResponse.renderer());
		Spark.post("/removeShare", "application/json", sharingController::removeShare, JsonResponse.renderer());
		Spark.get("/shared/:uuid/*", "application/json", sharingController::getShare, JsonResponse.renderer());
	}
}
