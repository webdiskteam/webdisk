package pl.rafsze.webdisk.utils;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import com.google.common.base.Preconditions;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.val;
import lombok.extern.slf4j.Slf4j;
import pl.rafsze.webdisk.WebDisk;
import spark.Redirect.Status;
import spark.Route;
import spark.resource.AbstractFileResolvingResource;
import spark.resource.ClassPathResource;
import spark.resource.ExternalResource;
import spark.staticfiles.MimeType;
import spark.utils.IOUtils;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Routes
{
	@Setter
	private static File externalResourcesFolder = null; // TODO: better way to handle static resources
	
	public static Route staticResource(@NonNull String path)
	{
		Preconditions.checkArgument(path.startsWith("/"), "Static resource path must start with slash");
		
		return (request, response) -> {
			
			log.debug("Request with uri {} for resource {}", request.uri(), path);
			
			AbstractFileResolvingResource resource;
			
			if ( externalResourcesFolder != null )
			{
				resource = new ExternalResource(new File(externalResourcesFolder, path).getAbsolutePath());
			}
			else
			{
				resource = new ClassPathResource("/app" + path, WebDisk.class.getClassLoader());
			}
			
			Preconditions.checkState(resource.exists() && resource.isReadable());
			
			response.type(MimeType.fromResource(resource));
			
			try ( OutputStream out = response.raw().getOutputStream(); InputStream in = resource.getInputStream() )
			{
				IOUtils.copy(in, out);
			}
			
			return null;
		};
	}
	
	public static Route redirect(String redirectTo)
	{
		return redirect(redirectTo, Status.FOUND);
	}
	
	public static Route redirect(@NonNull String redirectTo, @NonNull Status redirectStatus)
	{
		return (request, response) -> {	
		
			log.debug("Redirecting from {} to {} with status", request.uri(), redirectTo, redirectStatus);
			
			response.header("Location", redirectTo);
			response.header("Connection", "close");
			response.status(redirectStatus.intValue());

			return getRedirectHtml(redirectTo);
		};
	}
	
	@SneakyThrows
	private static String getRedirectHtml(final String location)
	{
		try ( val sw = new StringWriter(); val out = new PrintWriter(sw) )
		{
			out.println("<!DOCTYPE html>");
			out.print("<html><head><title>Redirect</title></head><body>Resource can be found at <a href=\"");
			out.print(location);
			out.print("\">");
			out.print(location);
			out.print("</a></body></html>");
			out.println();
			return sw.toString();
		}
	}
}
