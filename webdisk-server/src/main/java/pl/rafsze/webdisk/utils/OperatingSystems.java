package pl.rafsze.webdisk.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OperatingSystems
{
	public static boolean isWindows()
	{
		String osName = System.getProperty("os.name");
		return osName.substring(0, Math.min(osName.length(), 3)).equalsIgnoreCase("win");
	}
	
	public static String normalizePathForOperatingSystem(String path)
	{
		if ( !isWindows() )
		{
			return "/" + path;
		}
		
		return !path.endsWith("/") ? path + "/" : path;
	}
}
