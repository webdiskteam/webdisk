/*
	
Copyright (C) 2018-2019 WebDisk Team

This program is free software: you can redistribute it and/or modify
It under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>
    
*/
package pl.rafsze.webdisk;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import pl.rafsze.webdisk.response.JsonResponse;
import pl.rafsze.webdisk.user.User;
import spark.Request;
import spark.Response;
import spark.utils.IOUtils;

@Slf4j
@RequiredArgsConstructor
public class UploadApiHandler
{
	
	public Object handleUpload(Request request, Response response)
	{
		request.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement(System.getProperty("java.io.tmpdir")));
		User user = request.attribute("user");
		
		// user logged
		if(user == null)
		{
			return UploadInfoResponse.requireAuth();
		}
		
		String relativeDirectory = request.queryParams("directory");
		File uploadDir = new File(user.getHomeFolder(), relativeDirectory);
		
		if(user.isGuest())
		{
			if(!user.isHomeFolderWriteable())
			{
				return UploadInfoResponse.noWritePermissions();
			}
		}
		
		if(!uploadDir.exists())
		{
			if(!uploadDir.mkdirs())
			{
				return UploadInfoResponse.noWritePermissions();
			}
		}
		
		String fileName = null;
		try
		{
			fileName = getFileName(request.raw().getPart("uploaded_file"));
		}catch(Exception e)
		{
			log.error("Failed to obtain name of file.", e);
			return UploadInfoResponse.error();
		}
		Path tempFile;
		try
		{
			tempFile = File.createTempFile(fileName, ".tmp").toPath();
		}catch(IOException e)
		{
			log.error("Failed to create a temp file.", e);
			return UploadInfoResponse.error();
		}

        try (InputStream input = request.raw().getPart("uploaded_file").getInputStream()) { 
            Files.copy(input, tempFile, StandardCopyOption.REPLACE_EXISTING);
        }catch(Exception e)
		{
			log.error("Stream failed.", e);
			log.error("Servlet failed." ,e);
			try
			{
				Files.delete(tempFile);
			}catch(IOException e1)
			{
				e1.printStackTrace();
			}
			return UploadInfoResponse.error();
		}
        
        File targetFile = new File(uploadDir, fileName);
        try
		{
			Files.move(tempFile, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}catch(IOException e)
		{
			log.error("Unable to move uploaded file", e);
			return UploadInfoResponse.error();
		}
        
        logInfo(request, targetFile.toPath());
        
		return UploadInfoResponse.uploaded();
	}
	
	@SneakyThrows
	private static void logInfo(Request req, Path tempFile) {
        log.info("Uploaded file '{}' saved as '{}'", getFileName(req.raw().getPart("uploaded_file")), tempFile.toAbsolutePath());
    }

    private static String getFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                return cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
    
    @SneakyThrows
    public Object setFileContent(Request request, Response response)
    {
    	response.type("application/json");
		
		User user = request.attribute("user");
		if ( user == null )
		{
			return SetFileContentResponse.fileNotExist();
		}
		
		String path = request.splat().length == 0 ? "/" : request.splat()[0];
		path = path.replace('\\', '/');
		
		File file = user.getFile(path);
		
		if ( !user.isHomeFolderWriteable() || !file.isFile() || !file.canWrite() )
		{
			return SetFileContentResponse.fileNotExist();
		}
		
		try ( OutputStream out = new FileOutputStream(file); InputStream in = request.raw().getInputStream() )
		{
			IOUtils.copy(in, out);
		}
		
		return SetFileContentResponse.uploaded();
    }
	
    @Getter
    @ToString
    private static class SetFileContentResponse extends JsonResponse
    {
    	private final boolean uploaded;
    	private final boolean fileNotExist;
    	
		public SetFileContentResponse(boolean uploaded, boolean fileNotExist)
		{
			super(true);
			
			this.uploaded = uploaded;
			this.fileNotExist = fileNotExist;
		}
    	
		public static SetFileContentResponse uploaded()
		{
			return new SetFileContentResponse(true, false);
		}
		
		public static SetFileContentResponse fileNotExist()
		{
			return new SetFileContentResponse(false, true);
		}
    }
    
	@Getter
	@Setter
	@ToString
	private static class UploadInfoResponse extends JsonResponse
	{
		private final boolean requireAuth;
		private final boolean uploaded;
		private final boolean requireWritePermissions;
		
		public UploadInfoResponse(boolean requireAuth, boolean uploaded, boolean requireWritePermissions)
		{
			super(true);
			this.requireWritePermissions = requireWritePermissions;
			this.uploaded = uploaded;
			this.requireAuth = requireAuth;
		}
		
		public static UploadInfoResponse requireAuth()
		{
			return new UploadInfoResponse(true, false, false);
		}
		
		public static UploadInfoResponse uploaded()
		{
			return new UploadInfoResponse(false, true, false);
		}
		
		public static UploadInfoResponse error()
		{
			return new UploadInfoResponse(false, false, false);
		}
		
		public static UploadInfoResponse noWritePermissions()
		{
			return new UploadInfoResponse(false, false, true);
		}
		
	}
	
}
