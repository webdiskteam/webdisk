package pl.rafsze.webdisk;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import pl.rafsze.webdisk.response.JsonResponse;
import pl.rafsze.webdisk.user.User;
import spark.Request;
import spark.Response;
import spark.utils.IOUtils;

public class DownloadFileHandler
{
	@SneakyThrows
	public Object downloadFile(Request request, Response response)
	{
		User user = request.attribute("user");
		if ( user == null )
		{
			response.type("application/json");
			response.status(404);
			return DownloadFileErrorResponse.notFound();
		}
		
		String path = request.splat().length == 0 ? "/" : request.splat()[0];
		path = path.replace('\\', '/');
		
		File file = user.getFile(path);
		if ( !file.isFile() || !file.canRead() )
		{
			response.type("application/json");
			response.status(404);
			return DownloadFileErrorResponse.notFound();
		}
		
		response.type("application/octet-stream");
		response.header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
		response.header("Cache-Control", "no-cache, no-store, must-revalidate, max-age=0");
		
		try ( OutputStream out = response.raw().getOutputStream(); InputStream in = new FileInputStream(file) )
		{
			IOUtils.copy(in, out);
		}
		
		return null;
	}
	
	private static class DownloadFileErrorResponse extends JsonResponse
	{
		private boolean fileNotExist;
		
		private DownloadFileErrorResponse(boolean fileNotExist)
		{
			super(true);
			this.fileNotExist = fileNotExist;
		}
		
		public static DownloadFileErrorResponse notFound()
		{
			return new DownloadFileErrorResponse(true);
		}
	}
	
	@NoArgsConstructor
	@AllArgsConstructor
	private static class DownloadFileInfo
	{
		
		private String zipName;
		
		private String basePath;
		
		private List<String> files = new ArrayList<>();
		
	}
}
