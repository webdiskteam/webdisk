export class HistoryNode {
	constructor(parent, path, pathArray) {
		this.parent = parent
		this.child = null
		this.pathArray = pathArray
		this.path = path
	}

	get parent() {
		return this._parent
	}

	set parent(newParent) {
		this._parent = newParent
	}

	get child() {
		return this._child
	}

	set child(newChild) {
		this._child = newChild
	}

	get path() {
		return this._path
	}

	set path(newValue) {
		this._path = newValue
	}

	get pathArray() {
		return this._pathArray
	}

	set pathArray(newValue) {
		this._pathArray = newValue
	}

}