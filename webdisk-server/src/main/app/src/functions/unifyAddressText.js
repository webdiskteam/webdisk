export const unifyAddressText = (text) => {
	if(!text.startsWith('/')) {
		text = '/'+text
	}
	if(!text.endsWith('/')) {
		text = text+'/'
	}
	// replace backslash with slash
	text = text.replace(/\\/g, '/')

	// remove multiple slashes
	text = text.replace(/\/+/g, '/')

	return text
}