
const paragraph = '§'
const colors = {
	"4": "#aa0000",
	"c": "#ff5555",
	"6": "#ffaa00",
	"e": "#ffff55",
	"2": "#00aa00",
	"a": "#55ff55",
	"b": "#55ffff",
	"3": "#00aaaa",
	"1": "#0000aa",
	"9": "#5555ff",
	"d": "#ff55ff",
	"5": "#aa00aa",
	"f": "#ffffff",
	"7": "#aaaaaa",
	"8": "#555555",
	"0": "#000000"
}

const special = {
	"<" : "&#60;",
	">" : "&#62;",
	"\"" : "&#34;",
	" " : "&#32;",
	"\t" : "&#9;",
	"'" : "&#39;"
}

export const parseColorizedString = (text) => {
	let colorizedText = ""
	let colorCode = ''
	let colorHex = ""
	let isColorized = false;
	for(let i = 0; i < text.length; i++) {
		if(text.charAt(i) === paragraph) {
			if(isColorized) {
				colorizedText += "</font>"
			}
			colorCode = text.charAt(++i)
			if((colorHex = colors[colorCode]) != null) {
				colorizedText += "<font style='color: "+colorHex+"'>"
				isColorized = true
			}
		} else {
			let specialChar = ''
			if((specialChar = special[text.charAt(i)]) != null) {
				colorizedText += specialChar
			}else{
				colorizedText += text.charAt(i)
			}
		}
	}
	if(isColorized) {
		colorizedText += "</font>"
	}

	return colorizedText
}