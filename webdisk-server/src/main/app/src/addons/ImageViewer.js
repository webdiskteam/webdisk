import React, { Component } from 'react';
import '../index.css';
import ReactBnbGallery from 'react-bnb-gallery';

export class ImageViewer extends Component {

	state = {
		galleryOpened: true
	};

	getPhotoUrls = () => {
		var urls = [];
		var path = this.props.path;
		this.props.selectedFiles.map(function(file) {
			urls.push("/download/"+path+file.name);
		})
		return urls;
	}

	toggleGallery = () => {
		if(this.state.galleryOpened) {
			this.props.closeWindow();
		}
		this.setState(prevState => ({
		  galleryOpened: !prevState.galleryOpened
		}));
	}

  	render() {
		const urls = this.getPhotoUrls();
		return (
			<ReactBnbGallery
				show={this.state.galleryOpened}
				photos={urls}
				onClose={this.toggleGallery}
		  	/>
		)
  	}
}

export default ImageViewer
