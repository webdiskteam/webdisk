import React, { Component } from 'react'
import "../../node_modules/video-react/dist/video-react.css";
import ReactPlayer from 'react-player'

export class VideoPlayer extends Component {
  	render() {
		return (
            <div style={{display: "flex", height: "100%"}}>
                <ReactPlayer style={{margin: "0px auto"}} width="auto" height="100%" controls={true} url={"/download/"+this.props.path+this.props.selectedFiles[0].name} playing />
            </div>
		)
  	}

    componentDidMount() {
        this.props.switchTitle(this.props.selectedFiles[0].name);
    }

}

export default VideoPlayer
