import React, { Component } from 'react';
import AceEditor from 'react-ace';
import axios from 'axios';

export class TextEditor extends Component {

	state = {
		displayed: false,
		noTextMessage: "Loading content",
		text: ""
	}

	saveFile = () => {
		let content = this.refs.aceEditor.editor.getValue();
		axios.put('/setContent/' + this.props.path + this.props.selectedFiles[0].name, content).then(
			(response) => {
				if(response.data.success === true && response.data.uploaded === true) {
					console.log("saved")
				}else {
					console.log("error")
				}
			}
		)
	}

	reloadFile = () => {
		axios.get('/download' + this.props.path + this.props.selectedFiles[0].name, {responseType: 'text'}).then(
			(response) => {
				
				if (response.status == 200) {
						this.setState({
						text: response.data,
						displayed: true
					})
				} else {
					this.setState({
						text: "",
						noTextMessage: "This content is not available.",
						displayed: false
					})
				}
			})

	}

	componentDidMount() {
		this.props.switchTitle("Text Editor: " + this.props.path + this.props.selectedFiles[0].name);
		this.reloadFile();
	}

  	render() {
		return (
			<div style={{width: "100%", height: "100%"}}>
				<div style={this.state.displayed ? {display: "none"} : {textAlign: "center", color: "red", marginTop: "0px"}}>{this.state.noTextMessage}</div>
				<div style={this.state.displayed ? {width: "100%", height: "100%"} : {display: "none"}}>
					<AceEditor
						mode=""
						theme=""
						mode="xml"
						ref="aceEditor"
						width="100%"
						height="calc(100% - 35px)"
						name="aceEditor"
						value={this.state.text}
						editorProps={{$blockScrolling: true}}
					/>
					<button style={{height: "30px", float: "right", marginTop: "5px"}} onClick={(e) => {this.saveFile()}}>save</button>
					<button style={{height: "30px", float: "right", marginTop: "5px", marginRight: "5px"}} onClick={(e) => {this.reloadFile()}}>reload</button>
				</div>
			</div>
		)
	}
}

export default TextEditor
