import {textEditorIcon, imageViewerIcon, videoPlayerIcon, fileSharerIcon} from '../functions/imagePaths';
import TextEditor from './TextEditor';
import ImageViewer from './ImageViewer';
import VideoPlayer from './VideoPlayer';
import FileSharer from './FileSharer'

export const addons = [
	{
		name: "Text Editor", // displayed name
		icon: textEditorIcon, // path to icon
		component: TextEditor, // component to render, component will be provided
							   // with selected files array as prop (this.props.selectedFiles) 
							   // and path (this.props.path) and switchTitle function (this.props.switchTitle)
							   // and function closeWindow (this.props.closeWindow)
		extensions: ["", "txt", "csv", "rtf", "md"], // required extensions, empty extension means any file
		singleFile: true, // supports single file only, multiple files otherwise
		isFolder: false // supports files only
	},
	{
		name: "Image Viewer",
		icon: imageViewerIcon,
		component: ImageViewer,
		extensions: ["png", "jpg", "jpeg", "gif", "bmp"],
		singleFile: false,
		isFolder: false
	},
	{
		name: "Video Player",
		icon: videoPlayerIcon,
		component: VideoPlayer,
		extensions: ["mp4", "flv", "webm"],
		singleFile: true,
		isFolder: false
	},
	{
		name: "File Sharer",
		icon: fileSharerIcon,
		component: FileSharer,
		extensions: [""],
		singleFile: false,
		isFolder: true
	}

]