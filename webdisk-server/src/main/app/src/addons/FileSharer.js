import React, { Component } from 'react';
import '../index.css';
import File from '../components/File'
import Axios from 'axios';
import Popup from "reactjs-popup";


class FileSharer extends Component{

    constructor(props){
        super(props);
        this.addInput = this.addInput.bind(this);
    }

    state = {
        allowedUsers: [],
        usernames: [""],
        maxUsers: 10,
        shared: false,
        links: [

        ]
    }

    renderInputs = () => {
        return this.state.usernames.map((elem, i) => {
            return <input name={"user"+i} onChange={this.handleChange} value={this.state.usernames[i]}></input>;
        })
    }

    addInput() {
        if(this.state.usernames.length <= this.state.maxUsers){
            let usernamesCopy = this.state.usernames.slice();
            usernamesCopy.push("");
            this.setState({
                usernames: usernamesCopy
            })
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        let usernamesCopy = this.state.usernames.slice();
        usernamesCopy[event.target.name[event.target.name.length-1]] = event.target.value;
        this.setState({
            usernames: usernamesCopy,
        });
    }

    renderFiles = () => {
        return this.props.selectedFiles.map(
            (file, i) => { 
                return (
                <div className={"file-icon"}>
                    <File file={file} path={this.props.path} > </File>
                    <span>{file.name}</span>
                </div>
                )}
        )
    }

    renderLinks = () => {
        return (
            <div className="linksContainer">
                {this.state.links.map((link) => {
                    return (
                        <div>
                            {window.location.host + "/shared/" + link.uuid}
                        </div>
                    )
                })}
            </div>
        )
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if(this.state.usernames.length !== 0){
            for(let i = 0; i < this.props.selectedFiles.length; i++){
                let data = {
                    filePath: this.props.path + "/" + this.props.selectedFiles[i].name,
                    users: this.state.usernames.map(
                        (elem, i) => {
                            return {
                                username: elem,
                                writeable: false,
                            }
                        } 
                    ),
                    expires: -1
                }
                var name = this.props.selectedFiles[i].name;
                Axios.post("/shareFile", data).then(
                    (response) => {
                        if (response.data.success === true) {
                            this.setState(prevState => ({
                                links: [
                                    ...prevState.links, {uuid: response.data.uuid}
                                ],
                                shared: true
                            }))
                        }
                    }
                )
		    }

        }
        // this.props.closeWindow();
    }

    render(){
        if(!this.state.shared) {
            return(
                <div id="file-sharer">
                    <div id="file-sharer-users">
                        <button onClick={this.addInput}>
                        Add another user
                        </button>
                        <form onSubmit={this.handleSubmit}>
                            Users to share selected files with:

                            <div id="file-sharer-usernames-input-div">
                            {this.renderInputs()}
                            </div>
                            <button type="submit" className="btn btn-success" value="Submit" >
                                Share
                            </button>
                            
                        </form>
                    </div>
                    <div id="file-sharer-files">
                        {this.renderFiles()}
                    </div>
                    
                </div>
            )
        } else {
            return (
                this.renderLinks()
            )
        }
    }
}

export default FileSharer;