import React from 'react';
import '../index.css';

function NavigationButtons(props){
    if(props.view === 'icons'){
        return(
        <div className="container navigation-buttons">
            <button onClick={props.goUp} className="btn btn-dark">
            Wróć
            </button>
                <button
                    className="btn btn-dark"
                    onClick={props.changeView}
                    >
                    Widok Listy</button>
        </div>
        );
    }
    else if(props.view === 'list'){
        return(
        <div className="container navigation-buttons">
            <button onClick={props.logout} className="btn btn-dark">
            Wyloguj
            </button>
            <button onClick={props.goUp} className="btn btn-dark">
            Wróć
            </button>
                <button
                    className="btn btn-dark"
                    onClick={props.changeView}>
                    Widok Ikon
            </button>
        </div>
        )
    }
}

export default NavigationButtons;