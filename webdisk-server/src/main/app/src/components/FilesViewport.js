import React, { Component } from 'react'
import FileExplorer from './FileExplorer'

export class FilesViewport extends Component {
  render() {
	return (
	  <div className='filesViewport'>
		<FileExplorer fileRoot={this.props.fileRoot} path={this.props.path} pathArray={this.props.pathArray} updatePath={this.props.updatePath}/>
	  </div>
	)
  }
}

export default FilesViewport
