import React from 'react';
import '../index.css';
import {fileIcon} from '../functions/imagePaths'

class File extends React.Component {
    constructor(props){
        super(props);
    }
    
    state = {
        imageFileExtensions: ["png", "bmp", "gif", "jpg", "jpeg"]
    }

    render() {
        var icon = fileIcon;
        if(this.props.file !== undefined && this.props.path !== undefined) {
            if(this.state.imageFileExtensions.includes(this.props.file.extension)) {
                icon = "/download/"+this.props.path+this.props.file.name
            }
        }
        if(this.props.selected)
            return <img onClick={this.props.onFileClick} src={icon} className="file img-fluid imageThumb" alt={this.props.altText}/>;
        else 
            return <img onClick={this.props.onFileClick} src={icon} className="file img-fluid imageThumb" alt={this.props.altText}/>;
    }

}

export default File;
