﻿import React from 'react';
import '../index.css';
import File from './File'
import axios from 'axios';
import Folder from './Folder';
import NavigationButtons from './NavigationButtons';
import FileExplorerHeader from './FileExplorerHeader';
import {unifyAddressText} from '../functions/unifyAddressText';
import ActionList from './ActionList';

class FileExplorer extends React.Component {
    constructor(props) {
		super(props);
	}
	
	state = {
		view: 'icons',
		path: "",
		files: [],
		lastClickedIndex: -1
	}

    renderFiles() {
        if(this.state.files.length === 0)
			return (<span>It looks like there is nothing here!</span>);
		
        return this.state.files.map((file, i) => {

            if (file.directory === true){
                return (
                <div className={file.selected ? "file-icon file-selected" : "file-icon"}>
                    <Folder onDoubleClick={(e) => this.onDoubleFolderClick(e, i)} onFolderClick={(e) => this.onFileClick(e, i)}> </Folder>
                    <span>{this.shrinkString(file.name)}</span>
                </div>
                )}
            else {
                return (
                <div className={file.selected ? "file-icon file-selected" : "file-icon"}>
                    <File file={file} path={this.props.path} onFileClick={(e) => this.onFileClick(e, i)}> </File>
                    <span>{this.shrinkString(file.name)}</span>
                </div>
                )}
        }
        );
    }

    renderFilesAsList() {
        if(this.state.files.length === 0)
            return (<span>It looks like there is nothing here!</span>);
        return this.state.files.map((file, i) => {
            if (file.directory === true)
                return (
                <div class="file-list-entry-folder">
                    <Folder selected={file.selected} onFolderClick={(e) => this.onFileClick(e, i)}> </Folder>
                    <span>{file.name}</span>
                </div>
                );
            else
                return (
                    <div class="file-list-entry-file">
                        <File file={file} path={this.props.path} selected={file.selected} onFileClick={(e) => this.onFileClick(e, i)}> </File>
                        <span>{file.name}</span>
                        <span>{(new Date(file.modificationTime)).toUTCString()}</span>
                        <span>{this.formatBytes(file.size)}</span>
                    </div>
                );
        }
        );
    }

    changeView(){
        if(this.state.view === 'icons'){
            this.setState({
                view: 'list',
            })
        }
        else{
            this.setState({
                view: 'icons',
            })
        }
    }

    formatBytes(bytes, decimals = 2) {
        if (bytes === 0) return '0 Bytes';
    
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    
        const i = Math.floor(Math.log(bytes) / Math.log(k));
    
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    onFileClick(e, i) {
		let file = this.state.files[i];
		if(file.selected && !e.shiftKey && !e.ctrlKey && file.directory){
			let name = file.name;

			let unifiedPath = unifyAddressText(this.props.path + name);
			let textTrimmed = unifiedPath.substring(1, unifiedPath.length-1);
			this.props.updatePath(unifiedPath, textTrimmed.split('/'));
		}else {
			if(e.shiftKey){
				let fromIndex = this.state.lastClickedIndex > i ? i : this.state.lastClickedIndex;
				let toIndex = this.state.lastClickedIndex > i ? this.state.lastClickedIndex : i;
				if(this.state.lastClickedIndex == -1){
					fromIndex = 0;
				}
				for(let j = 0; j < this.state.files.length; j++){
					if(j >= fromIndex && j <= toIndex){
						this.state.files[j].selected = true;
					}else{
						this.state.files[j].selected = false;
					}
				}
			}else if(e.ctrlKey){
				file.selected = !file.selected;
				this.state.lastClickedIndex = i;
			}else{
				this.state.files.map((file) => {
					file.selected = false;
				});
				file.selected = !file.selected;
				this.state.lastClickedIndex = i;
			}

			
			this.forceUpdate();
		}

    }

	unselectAll(e) {
		this.state.files.map((file) => {
			file.selected = false;
		});
		this.forceUpdate();
	}

    shrinkString(string){
        if(string.length>12){
            return string.substring(0, 10) + '...';
        }
        return string;
    }

	getSelectedFiles(){
		let selectedFiles = [];
		this.state.files.map((file) => {
			if(file.selected){
				selectedFiles.push(file);
			}
		});
		return selectedFiles;
	}

    render() {
		if(this.state.path != this.props.path) {
			axios.get(this.props.fileRoot + this.props.path).then(
				(response) => {
					if (response.data.success === true) {
						let fileList = [];
						var idCounter = 0;
						if(response.data.file != null) {
							response.data.file.directoryContents.map(function(file){
								file.selected = false;
								let extension = "";
								if(file.name.includes('.')) {
									extension = file.name.substring(file.name.lastIndexOf(".")+1);
								}
								file.extension = extension;
								file.id = idCounter++;
								fileList.push(file);
							})
						}
						fileList.sort((file1, file2) => {
							if((file1.directory && file2.directory) || (!file1.directory && !file2.directory)) {
								if(file1.name > file2.name) {
									return 1
								}else if(file1.name == file2.name) {
									return 0
								}else {
									return -1
								}
							}
							if (file1.directory && !file2.directory) {
								return -1;
							}
							if (file2.directory && !file1.directory) {
								return 1;
							}
							return 0;
						})

						this.setState({
							files: fileList,
							path: this.props.path
						})
					}
	
				})
		}
		
        if (this.state.view === 'icons') {
            return (
                <div className="file-container-icons">
					{this.renderFiles()}
					<ActionList path={this.props.path} selectedFiles={this.getSelectedFiles()}></ActionList>
                </div>
            )
        }
        else 
            return (
                <div>
                    <div className="file-container-list" >
                        <div className="file-list-entry-header">
                            <span></span>
                            <span>Name</span>
                            <span>Modification Date</span>
                            <span>File Size</span>
                        </div>
						{this.renderFilesAsList()}
						<ActionList path={this.props.path} selectedFiles={this.getSelectedFiles()}></ActionList>
                    </div>
                </div>
        )
		
    }

}

export default FileExplorer;





