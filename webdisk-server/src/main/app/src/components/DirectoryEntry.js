import React, { Component } from 'react'
import DirectoryTree from './DirectoryTree'
import {folderIcon} from '../functions/imagePaths'
import {unifyAddressText} from '../functions/unifyAddressText'

export class DirectoryEntry extends Component {

	state = {
		unfolded: false,
	}

	selectDirectory(e){
		let unifiedPath = unifyAddressText(this.props.path)
		let textTrimmed = unifiedPath.substring(1, unifiedPath.length-1)
		this.props.updatePath(unifiedPath, textTrimmed.split('/'))
	}

	toggleDirectoryTreeView(e){
		this.setState({
			unfolded: !this.state.unfolded
		})
	}

	disableDirectoryTreeView(){
		if(this.state.unfolded){
			this.setState({
				unfolded: false
			})
		}
	}

	enableDirectoryTreeView(){
		if(this.state.unfolded){
			this.setState((state, props) => {
				return {unfolded: true}
			})
		}
	}

	getDirectoryTreeView(insideRealPath){
		return(
			<DirectoryTree fileRoot={this.props.fileRoot} pathArray={this.props.pathArray} level={this.props.level + 1} path={this.props.path} updatePath={this.props.updatePath} insideRealPath={insideRealPath}/>
		)
	}

	render() {
		let directoryTree;
		let unfoldedNow = false
		// if current directory path matches this folder
		let insideRealPath = this.props.insideRealPath
		let directoryClassName = 'directoryEntry'

		if(this.props.pathArray !== null){
			if(this.props.pathArray.length-1 >= this.props.level){
				if(this.props.name === this.props.pathArray[this.props.level] && this.props.insideRealPath){
					// last folder of current directory's path
					if(this.props.pathArray.length-1 === this.props.level){
						// highlights element
						directoryClassName = 'directoryEntry currentDirectoryEntry'
					}
					// unfold if directory tree belongs to current directory's path and is not last directory
					if(!this.state.unfolded && this.props.pathArray.length-1 !== this.props.level){
						this.enableDirectoryTreeView()
						unfoldedNow = true
					}
					insideRealPath = true;
				}else{
					insideRealPath = false;
				}
			}
		}

		if(this.state.unfolded || unfoldedNow)
		{
			directoryTree = this.getDirectoryTreeView(insideRealPath)
		}else{
			directoryTree = ''
		}

		let foldSymbol = '⟳'
		if(this.state.unfolded){
			foldSymbol = '↷'
		}

		return (
			<div className='directoryEntryContainer'>
				<div ref='entry' className={directoryClassName}>
					<div style={{float: 'left', cursor: 'pointer'}} onClick={(e)=>{this.toggleDirectoryTreeView(e)}}>{foldSymbol}{' '}</div>
					<div style={{float: 'left', paddingLeft: '10px', cursor: 'pointer'}} onClick={(e)=>{this.selectDirectory(e)}}>
						<img src={folderIcon} style={{height: '18px', paddingRight: '10px'}}></img>
					</div>
					<div style={{float: 'left', cursor: 'pointer'}} onClick={(e)=>{this.selectDirectory(e)}}>{this.props.name}</div>
				</div>
				{directoryTree}
			</div>
		)
	}
}

export default DirectoryEntry
