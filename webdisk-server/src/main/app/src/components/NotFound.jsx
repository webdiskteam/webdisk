import React from 'react';

function NotFound(props){
    return(<h1>sorry, it looks like this page doesn't exist!</h1>)
}

export default NotFound;