import React, { Component } from 'react'
import {addons} from '../addons/AddonRegistry';

export class ActionList extends Component {

	state = {
		selectedAddon: null
	}

	isAddonCompatibleWithSelectedFiles = (addon) => {
		if((addon.singleFile && this.props.selectedFiles.length == 1) || (!addon.singleFile && this.props.selectedFiles.length >= 1)) {
			const extensions = addon.extensions;
			for(let i = 0; i < this.props.selectedFiles.length; i++) {
				if(!extensions.includes(this.props.selectedFiles[i].extension) || (this.props.selectedFiles[i].directory && !addon.isFolder)){
					if(!(!this.props.selectedFiles[i].directory && extensions.includes(""))){
						return false;
					}
				}
			}
			return true;
		}
		return false;
	}

	renderAddons = () => {
		const selectAddon = this.selectAddon;
		const isAddonCompatibleWithSelectedFiles = this.isAddonCompatibleWithSelectedFiles;
		
		return (
			<div>
				{addons.map((addon) => {
					if(isAddonCompatibleWithSelectedFiles(addon)){
						return(
							<div onClick={(e) => {selectAddon(e, addon)}} className="addonItem">
								<img src={addon.icon} className="addonIcon"></img>
							</div>)
					}
				})}
			</div>
		)
	}

	closeAddon = () => {
		this.setState({
			selectedAddon: null
		})
	}

	changeAddonTitle = (title) => {
		let component = this.refs.addonTitleText;
		component.value = title;
	}

	renderAddonViewport = (path, selectedFiles, addon) => {
		if(this.state.selectedAddon !== null){
			var props = { selectedFiles: selectedFiles, path: path, switchTitle: this.changeAddonTitle, closeWindow: this.closeAddon};

			return(
				<div className="addonViewport">
					<div className="addonContainer">
						<div className="addonTitleBar">
							<div className="addonTitle">
								<img style={{float: "left", height: "30px"}}src={this.state.selectedAddon.icon}></img>
								<input disabled type="text" ref="addonTitleText" className="titleTitle"></input>
							</div>
							<button onClick={(e) => {this.selectAddon(e, null)}} className="addonExitButton">close</button>
						</div>
						<div className="addonComponentViewport">
							
							{React.createElement(addon.component, props)}
						</div>
					</div>
				</div>
			)
		}
	}

	render() {
		return (
			<div className="addonList" style={this.props.selectedFiles.length > 0 ? {display: "block"} : {display: "none"}}>
				{this.renderAddons()}
				{this.renderAddonViewport(this.props.path, this.props.selectedFiles, this.state.selectedAddon)}
			</div>)
	}

	selectAddon = (e, addon) => {
		this.setState({
			selectedAddon: addon
		});
	}
}

export default ActionList
