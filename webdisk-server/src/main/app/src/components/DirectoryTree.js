import React, { Component } from 'react'
import DirectoryEntry from './DirectoryEntry';
import axios from 'axios';

export class DirectoryTree extends Component {
	constructor(props) {
		super(props)
		
		axios.get(this.props.fileRoot + props.path).then(
            (response) => {
                if (response.data.success === true) {
					let folders = [];
					let id = 0
                    response.data.file.directoryContents.map(function(file) {
						if(file.directory){
							folders.push({
								id: id++,
								name: file.name
							})
						}
					})
					
					folders.sort(function(a,b){
						return a.name > b.name ? 1 : a.name == b.name ? 0 : -1
					})

					this.setState((state, props)=>{
						return({folders: folders})
					})
                }

            })

	}

	state = {
		folders : [
		]
	}

    render() {
		const root = this.props.root
		let rootTreeStyle = {}
		if(root){
			rootTreeStyle = {borderStyle: 'none'}
		}
		const pathArray = this.props.pathArray
		const insideRealPath = this.props.insideRealPath
		let path = this.props.path
		let updatePath = this.props.updatePath

		const folders = this.state.folders

        return (
			<ul style={rootTreeStyle}>
				{
					folders.map((folder) => (
						<li>
							<DirectoryEntry fileRoot={this.props.fileRoot} key={folder.id} name={folder.name} path={path+folder.name+'/'} pathArray={pathArray} level={this.props.level} insideRealPath={insideRealPath} updatePath={updatePath}/>
						</li>
					))
				}
			</ul>
        )
    }
}

export default DirectoryTree
