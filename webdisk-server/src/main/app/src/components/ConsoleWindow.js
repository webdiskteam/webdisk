import React, { Component } from 'react'
import '../index.css';
import {parseColorizedString} from '../functions/colorParser'
import {unifyAddressText} from '../functions/unifyAddressText'

export class ConsoleWindow extends Component {
	constructor(props) {
		super(props)
		this.appendLog = this.appendLog.bind(this)
	}

	state = {
		logs: [
		],
		text: "",
		scrollToBottom: false
	}

	appendLog = (path, content, pathVisible) => {
		this.setState(prevState => ({
			logs: [
				...prevState.logs, {path: path, content: content, pathVisible: pathVisible}
			],
			scrollToBottom: true
		}))
	}

	handleChange = (e) => {
		this.setState({
			text: e.target.value
		})
	}

	processCommand = (e) => {
		let content = this.refs.console.value

		var keyCode = e.keyCode || e.which;
		if(keyCode == 9) {
			e.preventDefault(); 
			console.log("Hello Tab");
		}

		if(e.key !== 'Enter' || content === "") {
			return
		}

		// check client commands
		if(content.startsWith("cd ") || content.startsWith("echo ") || content === "clear"){
			let command = content.split(' ')[0]
			if(command === "cd") {
				this.processCdCommand(content.substring(3, content.length))
			}else if(command === "echo") {
				this.appendLog(this.props.path, content, true)
				this.processEchoCommand(content.substring(5, content.length))
			}else if(content === "clear") {
				this.setState({
					logs: []
				})
			}
		// server commands
		}else{

			this.appendLog(this.props.path, content, true)

			var currentPath = this.props.path
			var appendLogFunc = this.appendLog

			// socket existed before
			if(socket != null) {
				// already open
				if(socket.readyState === 1)
				{
					socket.send(currentPath+"\0"+content)
				// closed 
				}else if(socket.readyState === 3 || socket.readyState === 2) {
					socket = new WebSocket("ws://"+window.location.hostname+":"+window.location.port+"/console")
					socket.onopen = function (event) {
						socket.send(currentPath+"\0"+content)
					}
					socket.onmessage = function(event) {
						appendLogFunc("", event.data, false)
					};
				}
			// socket never existed
			}else{
				var socket = new WebSocket("ws://"+window.location.hostname+":"+window.location.port+"/console")
				socket.onopen = function (event) {
					socket.send(currentPath+"\0"+content)
				}
				socket.onmessage = function(event) {
					appendLogFunc("", event.data, false)
				};
			}
			
		}

		this.setState({
			text: ""
		})
	}

	processCdCommand(path) {
		let realPath = path
		if(!path.startsWith("/")) {
			realPath = this.props.path+"/"+path
		}
		if(path === "..") {
			realPath = "/"+this.props.path.substring(0, this.props.path.substring(0, this.props.path.length-1).lastIndexOf('/')+1);
		}
		if(path === ".") {
			return;
		}
		let unifiedPath = unifyAddressText(realPath)
		let textTrimmed = unifiedPath.substring(1, unifiedPath.length-1)
		this.appendLog(this.props.path, "cd "+path, true)
		this.props.updatePath(unifiedPath, textTrimmed.split('/'))
	}

	processEchoCommand(message) {
		this.appendLog("", message, false)
	}

	renderLog(path, content, pathVisible) {
		if(pathVisible) {
			return(
				<div>
					<div className="logDirectory">{path}{":"}</div>
					<div dangerouslySetInnerHTML={{__html: parseColorizedString(content)}}></div>
				</div>
			)
		}else{
			return(
				<div>
					<div dangerouslySetInnerHTML={{__html: parseColorizedString(content)}}></div>
				</div>
			)
		}
		
	}

	renderLogs() {
		const logs = this.state.logs
		
		return(
			<div ref="logsContainer" style={{overflow: "auto", height: "200px"}}>
				{logs.map((log) => {
					return(
						<div className="logContainer">
							{this.renderLog(log.path, log.content, log.pathVisible)}
						</div>
					)
				})}
			</div>
		)
	}

	render() {
		return (
			<div className="consoleContainer" style={{maxHeight: "35%", display: "flex"}}>
				{this.renderLogs()}
				<div className="logContainer" style={{display: "flex", flexDirection: "row", height: "50px"}}>
					<div className="logDirectory">{this.props.path}{":"}</div>
					<textarea ref="console" onChange={this.handleChange} onKeyDown={this.processCommand} className="console" value={this.state.text.replace(/^\n|\n$/g, '')}></textarea>
				</div>
			</div>
		)
	}

	componentDidMount() {
		this.scrollToBottom();
	}
	
	componentDidUpdate() {
		this.scrollToBottom();
	}
	
	scrollToBottom() {
		if(this.state.scrollToBottom) {
			this.setState({
				scrollToBottom: false
			})
			const consoleComponent = this.refs.logsContainer;
			consoleComponent.scrollTop = consoleComponent.scrollTopMax;
		}
	}

}

export default ConsoleWindow
