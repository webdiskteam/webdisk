import React from 'react';
import '../index.css';
import {folderIcon} from '../functions/imagePaths'

function Folder(props){
    if(props.selected)
        return <img onClick={props.onFolderClick} src={folderIcon} className="file" alt={props.altText}/>;
    else 
        return <img onClick={props.onFolderClick} src={folderIcon} className="file" alt={props.altText}/>;
}

export default Folder;
