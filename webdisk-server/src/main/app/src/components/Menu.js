import React, { Component } from 'react'
import {leftArrowIcon, rightArrowIcon, upArrowIcon, homeIcon, logoutIcon} from '../functions/imagePaths'
import { throws } from 'assert';

export class Menu extends Component {
  	render() {
		let onGoBackFunction = function(){}
		let onGoForwardFunction = function(){}
		let defaultClassName = 'menuEntry'
		let leftArrowClassName = defaultClassName
		let rightArrowClassName = defaultClassName
		let homeClassName = defaultClassName
		let logoutClassName = "logoutIcon"

		if(this.props.hasPreviousNodeInHistory()) {
			onGoBackFunction = this.props.goBackInHistory
		}else{
			leftArrowClassName += ' opacityInactive' 
		}

		if(this.props.hasNextNodeInHistory()) {
			onGoForwardFunction = this.props.goForwardInHistory
		}else{
			rightArrowClassName += ' opacityInactive' 
		}
		
		return (
			<div className='menuContainer darkBorder'>
				<div style={{backgroundImage: 'url('+leftArrowIcon+')'}} onClick={onGoBackFunction} className={leftArrowClassName}></div>
				<div style={{backgroundImage: 'url('+rightArrowIcon+')'}} onClick={onGoForwardFunction} className={rightArrowClassName}></div>
				<div style={{backgroundImage: 'url('+homeIcon+')'}} onClick={this.props.resetHistory} className={homeClassName}></div>
				<div style={{backgroundImage: 'url('+logoutIcon+')'}} onClick={this.props.logoutFunction} className={logoutClassName}></div>
			</div>
		)
	}
}

export default Menu
