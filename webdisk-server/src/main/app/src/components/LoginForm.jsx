import React from 'react';
import '../index.css';
import { Link } from 'react-router-dom'
import {loginBackground} from '../functions/imagePaths'

class LoginForm extends React.Component{
    constructor(props){
        super(props);
        this.state = 
        {
            username: '',
            password: '',
            emptyInput: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //this.props.checkLogin();
    }

    handleChange(event){
        this.setState({
        [event.target.name]: event.target.value,
        emptyInput: false
        });
    }
        

    handleSubmit = event => {
        event.preventDefault();
        if(this.state.username.length===0 || this.state.password.length===0)
            this.setState({
                emptyInput: true
            })
        else{
            this.props.login(this.state.username, this.state.password);
        }
      }
    

      renderEmptyInputMessage(){
          if(this.state.emptyInput){
            return(<small id="emptyInput" className="form-text text-muted">Please enter your username and password!</small>)
          }
          else return(null);
      }

      


    render(){
        if(this.props.loggedIn){
            return('you are already logged in!');
        }
        else{
            //console.log("rendering");
        return(
            <div style={{backgroundImage: 'url('+loginBackground+')'
                        , height: '100%'
                        , width: '100%'
                        , overflow: 'hidden'
                        , backgroundRepeat: 'round'}
                        }>
                <div className="login-form-div" >
                Zaloguj się
                    <form onSubmit={this.handleSubmit} >
                        <div className="form-group">
                            <input type="text" name="username" id="login" placeholder="username" value={this.state.login} onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <input type="password" name="password" id="password" placeholder="password" value={this.state.password} onChange={this.handleChange} />
                            {this.renderEmptyInputMessage()}
                        </div>
                            <button type="submit" className="btn outline" value="Submit" >
                            Zaloguj się
                            </button>
                    </form>
                </div>
            </div>
        );
    }}
}

export default LoginForm;

//<Link to="/register">Nie masz konta? Zarejestruj się</Link>
//The DOM crashes when rendering this link after changes
