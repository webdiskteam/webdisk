import React, { Component } from 'react'
import {unifyAddressText} from '../functions/unifyAddressText'

export class SearchBar extends Component {

	state = {
		address: ''
	}

	changeAddress = (e) => {
		if(e.key === 'Enter') {
			// get search bar text
			let text = this.refs.searchBar.value

			// unify search bar text
			text = unifyAddressText(text)

			this.refs.searchBar.value = text

			let textTrimmed = text.substring(1, text.length-1)
			this.props.updatePath(text, textTrimmed.split('/'))
		}
	}

  	render() {
		if(this.state.address !== this.props.path) {
			this.setState((state, props)=>{
				return({address: this.props.path})
			})
		}
		return (
			<div className='searchBarContainer darkBorder'>
				<input ref='searchBar' onKeyPress={this.changeAddress} className='searchBar darkBorder' type='text'></input>
			</div>
		)
	}

	componentDidUpdate() {
		this.refs.searchBar.value = this.state.address
	}

}

export default SearchBar
