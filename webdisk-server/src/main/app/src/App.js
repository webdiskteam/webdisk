import React, { Component } from 'react';
import './index.css';
import './components/DirectoryTree'
import DirectoryTree from './components/DirectoryTree'
import FilesViewport from './components/FilesViewport'
import BottomPanel from './components/BottomPanel'
import SearchBar from './components/SearchBar'
import Menu from './components/Menu'
import {HistoryNode} from './classes/HistoryNode'
import ConsoleWindow from './components/ConsoleWindow'
import LoginForm from './components/LoginForm'
import sha256 from 'js-sha256';
import axios from 'axios';

class App extends Component {
	

	state = {
		historyNode: new HistoryNode(null, '/', ['/']),
		loggedIn: false,
	}

	componentDidMount() {
		this.checkLogin();
		axios.get('/profile')
		.then(
			(response) => {
				if (!response.data.isGuest){
					setInterval(this.checkLogin, 30000);
				}
			}
		)
	}

	hasNextNodeInHistory = () => {
		return this.state.historyNode.child === null ? false : true
	}

	hasPreviousNodeInHistory = () => {
		return this.state.historyNode.parent === null ? false : true
	}

	goBackInHistory = () => {
		if(this.state.historyNode.parent !== null) {
			this.setState((state, props)=>{
				return({historyNode: state.historyNode.parent})
			})
		}
	}

	goForwardInHistory = () => {
		if(this.state.historyNode.child !== null) {
			this.setState((state, props)=>{
				return({historyNode: state.historyNode.child})
			})
		}
	}

	resetHistory = () => {
		this.setState((state, props)=>{
			return({historyNode: new HistoryNode(null, '/', ['/'])})
		})
	}

	updateCurrentDirectory = (path, pathArray) => {
		if(this.state.historyNode.path === path) {
			return
		}
		let stateHistoryNode = this.state.historyNode
		let newHistoryNode = new HistoryNode(stateHistoryNode, path, pathArray)
		stateHistoryNode.child = newHistoryNode

		this.setState((state, props)=>{
			return {
				historyNode: newHistoryNode
			}
		})
	}


	renderApp = (currentDirectoryPathArray, currentDirectoryPath) => {
		var url = window.location.pathname;
		var args = url.split('/')
		var type = args[1]
		var uuid = args.length >= 3 ? args[2] : "";

		var fileRoot = '/files'

		if(type === 'shared') {
			fileRoot = '/shared/' + uuid;
		}

		return (
			<div className='mainContainer'>
				<div className='centerContainer'>
					<div className='sidePanel lightBorder'>
						<div>
							<Menu hasNextNodeInHistory={this.hasNextNodeInHistory} hasPreviousNodeInHistory={this.hasPreviousNodeInHistory} goBackInHistory={this.goBackInHistory} 
							goForwardInHistory={this.goForwardInHistory} resetHistory={this.resetHistory} logoutFunction={this.logout}/>
						</div>
						<div style={{overflow: 'auto', flex: 'auto'}}>
							<DirectoryTree root={true} fileRoot={fileRoot} path='/' pathArray={currentDirectoryPathArray} level={0} insideRealPath={true} updatePath={this.updateCurrentDirectory}/>
						</div>
					</div>
					<div className='filesPanel lightBorder'>
						<div>
							<SearchBar path={currentDirectoryPath} updatePath={this.updateCurrentDirectory} />
						</div>
						<div style={{overflow: "auto", height: "100%"}}>
							<div className="filesViewportContainer">
								<FilesViewport fileRoot={fileRoot} path={currentDirectoryPath} pathArray={currentDirectoryPathArray} updatePath={this.updateCurrentDirectory}/>
							</div>
						</div>
						<ConsoleWindow path={currentDirectoryPath} pathArray={currentDirectoryPathArray} updatePath={this.updateCurrentDirectory}/>
					</div>
				</div>
				<div className='bottomPanel'>
					<BottomPanel />
				</div>
			</div>
		)
		
	}

	checkLogin = () => {
		axios.get('/profile')
		.then(
			(response) => {
				console.log(response.data)
				console.log(response.data.userProfile)
				/*
				if(response !== undefined && response.data.userProfile !== undefined && this.state.loggedIn === false){
					this.setState({
						loggedIn: true
					});
				}
				
				else */
				if(response === undefined || response.data.userProfile === undefined){
					this.setState({
						loggedIn: false
					});
				}
				else if(this.state.loggedIn === false){
					this.setState({
						loggedIn: true
					});
				}
			}
		)
	}

	login = (username, password) => {
		/*
		if(username === 'admin' && password ==='admin'){
		  this.setState({
			loggedIn: true
		   });
		}
		else
		*/
		
		{
		axios.post(`/login`, { 
		  username: username,
		  password: sha256(password)
	   }).then(res =>{
		 if(res.data.loginSuccess === true){
		   this.setState({
			loggedIn: true
		   })
		 }
	   })
	  }
	}


	logout = () => {
		axios.post('/logout').then(
		  (response) => {
			  console.log(response.data)
			this.setState({
			  loggedIn: false
			})
		  }
		)
	  }
	

	render() {
		const currentDirectoryPathArray = this.state.historyNode.pathArray
		const currentDirectoryPath = this.state.historyNode.path
		if(this.state.loggedIn)
		{
			return this.renderApp(currentDirectoryPathArray, currentDirectoryPath);

		}
		else{
			return <LoginForm login={this.login} checkLogin={this.checkLogin} loggedIn={this.state.loggedIn}/>
		}
	}

}

export default App;
