package pl.rafsze.webdisk.api.plugin;

/**
 * This class is used to initialize/cleanup plugin enviroment
 * 
 * To use this class you should provide a service entry in META-INF/services
 * or annotate this with ProvidesService annotation
 */
public abstract class PluginInitializer
{
	/**
	 * This method is called once when plugin is loaded
	 */
	public void initialize()
	{
	}
	
	/**
	 * This method is called once when plugin is unloaded
	 */
	public void cleanup()
	{
	}
}
