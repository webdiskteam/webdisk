package pl.rafsze.webdisk.api;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.rafsze.webdisk.api.command.CommandHandler;
import pl.rafsze.webdisk.api.user.User;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class WebDiskApi
{
	@Getter
	private static WebDiskApi instance;
	
	public abstract User getUser(String username);
	
	public abstract void registerCommandHandler(CommandHandler commandHandler);
}
