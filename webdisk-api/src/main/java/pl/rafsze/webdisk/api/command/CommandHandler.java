package pl.rafsze.webdisk.api.command;

public abstract class CommandHandler
{
	public abstract void execute(CommandContext ctx);
}
