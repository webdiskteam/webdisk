package pl.rafsze.webdisk.api.command;

import java.io.InputStream;
import java.io.PrintStream;

import pl.rafsze.webdisk.api.user.User;

public interface CommandContext
{
	User getUser();
	
	String getCommandName();
	
	String getCommandLine();

	int getArgsCount();
	
	String[] getArgs();

	String getArg(int index);
	
	void halt();
	
	InputStream getInputStream();
	
	PrintStream getOutputStream();
	
	PrintStream getErrorStream();
}
