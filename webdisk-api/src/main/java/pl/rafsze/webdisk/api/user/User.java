package pl.rafsze.webdisk.api.user;

import java.io.File;

public interface User
{
	String getUsername();
	
	File getHomeFolder();
	
	default File getFile(String path)
	{
		return new File(getHomeFolder(), path);
	}
}
